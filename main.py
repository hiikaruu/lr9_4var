def func_n(N, t=0, F=0):
    if N < 10:
        F = F * 10 + N
        print("\n", N)
        return F
    else:
        t = N % 10
        N = N // 10
        F= F*10 + t
        print("\n", t)
        return func_n(N, t, F)

if __name__ == '__main__':
    N = int(input("Введите натуральное число "))
    if N > 0:
      A = func_n(N)
    else:
        print("Не соответствует")
