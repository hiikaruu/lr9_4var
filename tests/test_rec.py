from main import func_n
import pytest

@pytest.mark.parametrize("N, F", [(336, 633), (8889, 9888), (3 , 3)])


def test_fun(N, F):
    assert func_n(N) == F

